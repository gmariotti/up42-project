package challenge

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.actuate.metrics.AutoConfigureMetrics
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.http.MediaType.*
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.returnResult

@SpringBootTest(webEnvironment = RANDOM_PORT)
@ActiveProfiles("local", "test")
@AutoConfigureMetrics
class ApplicationTest {
    @Autowired
    lateinit var client: WebTestClient

    @ParameterizedTest
    @ValueSource(
        strings = ["/health/liveness", "/health/readiness", "/prometheus"]
    )
    fun `verify expected endpoints are exposed`(endpoint: String) {
        client
            .get()
            .uri(endpoint)
            .exchange()
            .expectStatus().is2xxSuccessful
    }

    @Test
    fun `get list of features available`() {
        val result = client
            .get()
            .uri("features")
            .exchange()
            .expectStatus().is2xxSuccessful
            .expectHeader().contentType(APPLICATION_JSON)
            .returnResult<Map<String, Any>>()
            .responseBody
            .collectList()
            .block()!!

        assertThat(result).isNotEmpty
        assertThat(result)
            .usingRecursiveFieldByFieldElementComparator()
            .isNotEmpty
            .allSatisfy {
                assertThat(it).containsKeys(
                    "id",
                    "missionName",
                    "timestamp",
                    "beginViewingDate",
                    "endViewingDate"
                )
            }
    }

    @Test
    fun `get one of features available`() {
        val id = "cf5dbe37-ab95-4af1-97ad-2637aec4ddf0"

        client
            .get()
            .uri("features/$id")
            .exchange()
            .expectStatus().is2xxSuccessful
            .expectHeader().contentType(APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id").isEqualTo(id)
            .jsonPath("$.missionName").isEqualTo("Sentinel-1B")
            .jsonPath("$.timestamp").isNumber
            .jsonPath("$.beginViewingDate").isNumber
            .jsonPath("$.endViewingDate").isNumber
    }

    @Test
    fun `get one of the quicklooks`() {
        val id = "63fded50-842f-4384-ac65-e83d584beb4c"

        val result = client
            .get()
            .uri("features/$id/quicklook")
            .exchange()
            .expectStatus().is2xxSuccessful
            .expectHeader().contentType(IMAGE_PNG)
            .expectBody()
            .returnResult()
            .responseBody

        val expected = javaClass.classLoader.getResourceAsStream("img/$id.png")!!.readAllBytes()
        assertThat(expected).isEqualTo(result)
    }
}
