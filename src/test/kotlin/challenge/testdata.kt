package challenge

import java.util.*

val quicklook = Base64.getEncoder().encode("quicklook".toByteArray()).decodeToString()
val featureId = FeatureId("id")
val acquisition = Acquisition("missionName", endViewingDate = 3, beginViewingDate = 2)
val properties = Properties(featureId, timestamp = 1, acquisition, quicklook)
val feature = Feature(properties)
val featureWithoutQuicklook = Feature(properties.copy(quicklook = null))
val collection = FeatureCollection(listOf(feature))
val response = feature.toFeatureResponse()