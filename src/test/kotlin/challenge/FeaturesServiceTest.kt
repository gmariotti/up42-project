package challenge

import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class FeaturesServiceTest {
    private val features = listOf(collection, collection)
    private val client = mockk<FeatureClient> {
        coEvery { getFeatures() } returns features
    }

    @Test
    fun `list of features is properly mapped`() {
        runBlocking {
            val service = FeaturesService(client)

            val result = service.getFeatures()

            assertThat(result).isEqualTo(listOf(response, response))
        }
    }

    @Test
    fun `return feature if found in the list`() {
        runBlocking {
            val service = FeaturesService(client)

            val result = service.getFeature(properties.id)

            assertThat(result).isEqualTo(response)
        }
    }

    @Test
    fun `return null if feature can't be found`() {
        runBlocking {
            val service = FeaturesService(client)

            val result = service.getFeature(FeatureId("another-id"))

            assertThat(result).isNull()
        }
    }

    @Test
    fun `return quicklook if feature is found`() {
        runBlocking {
            val service = FeaturesService(client)

            val result = service.getQuicklook(properties.id)

            assertThat(result).isEqualTo(QuicklookResponse("quicklook".toByteArray()))
        }
    }

    @Test
    fun `return null if feature can't be found when searching for quicklook`() {
        runBlocking {
            val service = FeaturesService(client)

            val result = service.getQuicklook(FeatureId("another-id"))

            assertThat(result).isNull()
        }
    }

    @Test
    fun `return empty response if feature has no quicklook`() {
        runBlocking {
            coEvery { client.getFeatures() } returns listOf(FeatureCollection(listOf(featureWithoutQuicklook)))
            val service = FeaturesService(client)

            val result = service.getQuicklook(properties.id)

            assertThat(result).isEqualTo(QuicklookResponse())
        }
    }
}
