package challenge

import challenge.configurations.ProblemConfiguration
import com.ninjasquad.springmockk.MockkBean
import io.mockk.coEvery
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.context.annotation.Import
import org.springframework.test.web.reactive.server.WebTestClient
import org.zalando.problem.violations.ConstraintViolationProblem

@WebFluxTest(controllers = [FeaturesController::class])
@Import(ProblemConfiguration::class)
class FeaturesControllerTest {
    @Autowired
    lateinit var client: WebTestClient
    @MockkBean
    lateinit var service: FeaturesService

    @Nested
    inner class FeaturesTest {
        private val endpoint = "/features"

        @Test
        fun `get list of features`() {
            coEvery { service.getFeatures() } returns listOf(response)

            client
                .get()
                .uri(endpoint)
                .exchange()
                .expectStatus().is2xxSuccessful
                .expectBody()
                .jsonPath("$").isArray
                .jsonPath("$[0].id").isEqualTo(response.id.value)
                .jsonPath("$[0].missionName").isEqualTo(response.missionName)
                .jsonPath("$[0].timestamp").isEqualTo(response.timestamp)
                .jsonPath("$[0].beginViewingDate").isEqualTo(response.beginViewingDate)
                .jsonPath("$[0].endViewingDate").isEqualTo(response.endViewingDate)
        }

        @Test
        fun `get empty list of features`() {
            coEvery { service.getFeatures() } returns emptyList()

            client
                .get()
                .uri(endpoint)
                .exchange()
                .expectStatus().is2xxSuccessful
                .expectBody()
                .jsonPath("$").isEmpty
        }
    }

    @Nested
    inner class FeatureTest {
        private val endpoint = "/features/$featureId"

        @Test
        fun `return feature if it exists`() {
            coEvery { service.getFeature(featureId) } returns response

            client
                .get()
                .uri(endpoint)
                .exchange()
                .expectStatus().is2xxSuccessful
                .expectBody()
                .jsonPath("$.id").isEqualTo(response.id.value)
                .jsonPath("$.missionName").isEqualTo(response.missionName)
                .jsonPath("$.timestamp").isEqualTo(response.timestamp)
                .jsonPath("$.beginViewingDate").isEqualTo(response.beginViewingDate)
                .jsonPath("$.endViewingDate").isEqualTo(response.endViewingDate)
        }

        @Test
        fun `return 404 if feature id is not present`() {
            coEvery { service.getFeature(featureId) } returns null

            client
                .get()
                .uri(endpoint)
                .exchange()
                .expectStatus().isNotFound
                .expectBody()
                .jsonPath("$.id").isEqualTo(response.id.value)
                .jsonPath("$.type").isEqualTo("/problems/feature-not-existent")
        }

        @Test
        fun `return 400 if feature id field is invalid`() {
            client
                .get()
                .uri(endpoint.replace(featureId.value, " "))
                .exchange()
                .expectStatus().isBadRequest
                .expectBody()
                .jsonPath("$.type").isEqualTo(ConstraintViolationProblem.TYPE_VALUE)
        }
    }

    @Nested
    inner class QuicklookTest {
        private val endpoint = "/features/$featureId/quicklook"

        @Test
        fun `return feature if it exists`() {
            coEvery { service.getQuicklook(featureId) } returns QuicklookResponse("result".toByteArray())

            val response = client
                .get()
                .uri(endpoint)
                .exchange()
                .expectStatus().is2xxSuccessful
                .expectBody()
                .returnResult().responseBody.decodeToString()

            assertThat(response).isEqualTo("result")
        }

        @Test
        fun `return 404 if feature id is not present`() {
            coEvery { service.getQuicklook(featureId) } returns null

            client
                .get()
                .uri(endpoint)
                .exchange()
                .expectStatus().isNotFound
                .expectBody()
                .jsonPath("$.id").isEqualTo(response.id.value)
                .jsonPath("$.type").isEqualTo("/problems/feature-not-existent")
        }

        @Test
        fun `return 404 if quicklook is not present`() {
            coEvery { service.getQuicklook(featureId) } returns QuicklookResponse()

            client
                .get()
                .uri(endpoint)
                .exchange()
                .expectStatus().isNotFound
                .expectBody()
                .jsonPath("$.id").isEqualTo(response.id.value)
                .jsonPath("$.type").isEqualTo("/problems/quicklook-not-existent")
        }

        @Test
        fun `return 400 if feature id field is invalid`() {
            client
                .get()
                .uri(endpoint.replace(featureId.value, " "))
                .exchange()
                .expectStatus().isBadRequest
                .expectBody()
                .jsonPath("$.type").isEqualTo(ConstraintViolationProblem.TYPE_VALUE)
        }
    }
}