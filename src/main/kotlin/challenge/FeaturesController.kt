package challenge

import org.springframework.http.MediaType.IMAGE_PNG_VALUE
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController
class FeaturesController(private val service: FeaturesService) {

    @GetMapping("/features")
    suspend fun getFeatures(): List<FeatureResponse> = service.getFeatures()

    @GetMapping("/features/{id}")
    suspend fun getFeature(@PathVariable id: FeatureId): FeatureResponse? {
        if (id.value.isBlank()) throw featureIdViolationProblem(id)
        return service.getFeature(id) ?: throw FeatureNotFoundException(id)
    }

    @GetMapping("/features/{id}/quicklook", produces = [IMAGE_PNG_VALUE])
    suspend fun getQuicklook(@PathVariable id: FeatureId): ByteArray? {
        if (id.value.isBlank()) throw featureIdViolationProblem(id)
        val quicklook = service.getQuicklook(id)
        return when {
            quicklook == null -> throw FeatureNotFoundException(id)
            quicklook.image == null -> throw QuicklookNotFoundException(id)
            else -> quicklook.image
        }
    }
}