package challenge

import org.zalando.problem.AbstractThrowableProblem
import org.zalando.problem.Exceptional
import org.zalando.problem.Status
import org.zalando.problem.violations.ConstraintViolationProblem
import org.zalando.problem.violations.Violation
import java.net.URI

class FeatureNotFoundException(val id: FeatureId) : AbstractThrowableProblem(
    URI.create("/problems/feature-not-existent"),
    "Feature does not exist",
    Status.NOT_FOUND,
    "No feature found for id = $id"
) {
    override fun getCause(): Exceptional? = null
}

class QuicklookNotFoundException(val id: FeatureId) : AbstractThrowableProblem(
    URI.create("/problems/quicklook-not-existent"),
    "Quicklook does not exist",
    Status.NOT_FOUND,
    "No quicklook found for feature with id = $id"
) {
    override fun getCause(): Exceptional? = null
}

fun featureIdViolationProblem(id: FeatureId) = ConstraintViolationProblem(
    Status.BAD_REQUEST,
    listOf(Violation("id", "id field is empty or blank"))
)