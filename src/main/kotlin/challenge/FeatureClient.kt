package challenge

interface FeatureClient {
    suspend fun getFeatures(): List<FeatureCollection>
}
