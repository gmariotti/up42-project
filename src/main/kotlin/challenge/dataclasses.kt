package challenge

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonCreator.Mode.DELEGATING
import com.fasterxml.jackson.annotation.JsonValue

data class FeatureCollection(val features: List<Feature>)

data class FeatureId @JsonCreator(mode = DELEGATING) constructor(@JsonValue val value: String) {
    override fun toString(): String = value
}

data class Feature(val properties: Properties)

data class Properties(
    val id: FeatureId,
    val timestamp: Long,
    val acquisition: Acquisition,
    val quicklook: String? = null
)
data class Acquisition(val missionName: String, val endViewingDate: Long, val beginViewingDate: Long)
