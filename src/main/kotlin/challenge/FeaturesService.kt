package challenge

import org.springframework.stereotype.Service
import java.util.*

@Service
class FeaturesService(private val client: FeatureClient) {
    suspend fun getFeatures(): List<FeatureResponse> = client
        .extractFeatures()
        .map { it.toFeatureResponse() }
        .toList()

    private suspend fun FeatureClient.extractFeatures() = getFeatures().asSequence().flatMap { it.features }

    suspend fun getFeature(id: FeatureId): FeatureResponse? = client
        .extractFeatures()
        .firstOrNull { it.properties.id == id }
        ?.toFeatureResponse()

    suspend fun getQuicklook(id: FeatureId): QuicklookResponse? {
        val feature = client.extractFeatures().firstOrNull { it.properties.id == id }
        return when {
            feature == null -> null
            feature.properties.quicklook == null -> QuicklookResponse()
            else -> QuicklookResponse(Base64.getDecoder().decode(feature.properties.quicklook))
        }
    }
}

data class FeatureResponse(
    val id: FeatureId,
    val timestamp: Long,
    val beginViewingDate: Long,
    val endViewingDate: Long,
    val missionName: String
)

data class QuicklookResponse(val image: ByteArray? = null) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as QuicklookResponse

        if (image != null) {
            if (other.image == null) return false
            if (!image.contentEquals(other.image)) return false
        } else if (other.image != null) return false

        return true
    }

    override fun hashCode(): Int {
        return image?.contentHashCode() ?: 0
    }
}

fun Feature.toFeatureResponse() = with(properties) {
    FeatureResponse(
        id,
        timestamp = timestamp,
        beginViewingDate = acquisition.beginViewingDate,
        endViewingDate = acquisition.endViewingDate,
        acquisition.missionName
    )
}