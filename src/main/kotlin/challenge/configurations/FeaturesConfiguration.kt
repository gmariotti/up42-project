package challenge.configurations

import challenge.FeatureClient
import challenge.mocks.LocalFeatureClient
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Profile
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotBlank

@Validated
@ConstructorBinding
@ConfigurationProperties("features")
data class FeaturesConfiguration(@field:NotBlank val filename: String) {

    @Bean @Profile("local")
    fun featureClient(mapper: ObjectMapper): FeatureClient {
        val file = javaClass.classLoader.getResource(filename)!!.file
        return LocalFeatureClient(file, mapper)
    }
}