package challenge.mocks

import challenge.FeatureClient
import challenge.FeatureCollection
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import java.nio.file.Paths

class LocalFeatureClient(jsonFile: String, mapper: ObjectMapper) : FeatureClient {
    private val features = Paths.get(jsonFile).toFile().let {
        mapper.readValue<List<FeatureCollection>>(it)
    }

    override suspend fun getFeatures(): List<FeatureCollection> = features
}