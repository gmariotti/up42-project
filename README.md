# UP42 Challenge

## Implementation details
I used Spring Webflux to leverage its high I/O performances with low resources consumption, that is usually the main 
bottleneck when implementing APIs that just serve data from somewhere else, together with Kotlin coroutines to make it 
easier to read the code.

I treated the data source as an API that always returns the entire list of features, in a different scenario I would
hope for pagination to be present, or to be able to filter the API directly by feature. The mock associated to the
`local` profile is used to distinguish between local development, when the API doesn't need to be queried, from the
production-like environments, where the API is available.

A couple of things I want to put evidence on:
- I use a data class wrapper together with Jackson annotations (see `FeatureId`) to avoid passing around a string all
the time and make sure that trivial bugs, like passing the wrong string, are caught at compile-time.
- The only integration test is the one in the `ApplicationTest`, while the rest are unit tests.
- In the `FeatureController` tests, I used Junit `@Nested` to group tests by endpoint. I find this to make it easy to
read the results in the IDE and to navigate the code.
- Getting the quicklook of a feature was done in the `FeaturesService`, but, in a real world scenario where the business
logic might be more complicated, I would actually move it into a different service.
- I used the `application/problem-json` for error messages, together with a Zalando library designed for Spring Boot. 
This gives nice standardized responses in case of error scenarios that makes it clear for the consumer of the API what
went wrong. An example would be a 404 on the quicklook endpoint. Is the 404 because the feature doesn't exist or because
the quicklook is missing? With `application/problem+json` this is clear.

## Run locally

### IntelliJ

Create a `Spring Boot` configuration and set profile to `local`.

### Docker

The image can be built locally with `

    $ ./gradlew jibDockerBuild

The image can be run locally with

    $ docker run --rm -p 8080:8080 -e SPRING_PROFILES_ACTIVE=local -it up42-project
